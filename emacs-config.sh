#! /usr/bin/env bash

set -e

if [ -d ~/.emacs.d ]; then
    rm -rf ~/.emacs.d
fi

git clone https://gitlab.com/sbannier/emacs-config.git ~/.emacs.d
