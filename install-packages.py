
import subprocess

"""
/var/log/apt/history.log:Commandline: apt-get install -y    libgccjit-12-dev 
/var/log/apt/history.log:Commandline: apt-get install libsecret-1-0 libsecret-1-dev libglib2.0-dev
/var/log/apt/history.log:Commandline: apt-get install --reinstall git
/var/log/apt/history.log:Commandline: apt-get install --reinstall git
/var/log/apt/history.log:Commandline: apt-get install git
/var/log/apt/history.log:Commandline: apt-get install git
/var/log/apt/history.log:Commandline: apt-get install --reinstall git
/var/log/apt/history.log:Commandline: apt-get install --install-recommends git
/var/log/apt/history.log:Commandline: apt-get install git
/var/log/apt/history.log:Commandline: apt-get install zstd
/var/log/apt/history.log:Commandline: apt-get install git
/var/log/apt/history.log:Commandline: apt-get install xterm
/var/log/apt/history.log:Commandline: apt-get install docker
/var/log/apt/history.log:Commandline: apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
/var/log/apt/history.log:Commandline: apt-get install podman
/var/log/apt/history.log:Commandline: apt-get install pandoc
/var/log/apt/history.log:Commandline: apt-get install python3.10-venv
/var/log/apt/history.log:Commandline: apt-get install traceroute
/var/log/apt/history.log:Commandline: apt-get install gitweb
/var/log/apt/history.log:Commandline: apt-get install lighttpd
/var/log/apt/history.log:Commandline: apt-get install docker-compose
/var/log/apt/history.log:Commandline: apt-get install openjdk-11-jdk bazel-bootstrap
/var/log/apt/history.log:Commandline: apt-get install bash-completion
/var/log/apt/history.log:Commandline: apt-get install maven
"""


packages = {
    "autoconf": {},  # for emacs
    "bash-completion": {},
    "texinfo": {},   # for makeinfo
    "global": {},
    "cmake": {},
    "evolution": {},
    "gimp": {},
    "git": {},
    "gparted": {},
    "leiningen": {},
    "perl-critic": {"manjaro": "perl-perl-critic", "ubuntu": "libperl-critic-perl"},
    "hunspell": {},
    "hunspell en us": {"manjaro": "hunspell-en_us", "ubuntu": "hunspell-en-us"},
    "imagemagick": {"manjaro": "imagemagick", "ubuntu": ["imagemagick", "libmagickcore-dev", "libmagickwand-dev"]},
    "jansson": {"manjaro": "jansson", "ubuntu": "libjansson-dev"},
    "build tools": {"manjaro": "base-devel", "ubuntu": "build-essential"},
    "jdk": {"manjaro": "jdk-openjdk", "ubuntu": "default-jdk"},
    "kdiff3":  {},
    "keepassxc": {},
    "lame": {},
    "libgccjit": {"manjaro": "lingccjit", "ubuntu": "libgccjit-13-dev"},
    "libvterm": {"manjaro": "libvterm", "ubuntu": "libvterm-dev"},
    "meld": {},
    "mercurial": {},
    "ripgrep": {},
    "subversion": {},
    "Python pip": {"manjaro": "python-pip", "ubuntu": "python3-pip"},
    "udiskie": {},
    "yamllint": {},
    "libgtkd-3-dev": {"ubuntu": "libgtkd-3-dev"},
    "libgif-dev": {"ubuntu": "libgif-dev"},
    "libxpm": {"ubuntu": "libxpm-dev"},
    "libgnutls28-dev": {"ubuntu": "libgnutls28-dev"},
    "libncurses": {"ubuntu": "libncurses-dev"},
    "silver searcher": {"manjaro": "the_silver_searcher", "ubuntu": "silversearcher-ag"},
    "font awesomw": {"manjaro": "ttf-font-awesome", "ubuntu": "fonts-font-awesome"},
    "font IBM plex": {"manjaro": "ttf-ibm-plex", "ubuntu": "fonts-ibm-plex"},


#     bash-language-server \
#     python-lsp-server \
#     python-pylint \
#     python-virtualenvwrapper \
    }

def install():
    to_install = ["sudo", "apt-get", "install", "-y"]

    for name, os_specific in packages.items():
        if os_specific:
            p = os_specific["ubuntu"]
            if isinstance(p, list):
                to_install.extend(p)
            else:
                to_install.append(p)
        else:
            to_install.append(name)

    subprocess.check_call(to_install)
    print(to_install)

if __name__ == "__main__":
    install()
