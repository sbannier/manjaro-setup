# Setting up a new Manjaro installation with all my tools #

# Run #

``` shell
bash install-packages.sh
bash emacs-config.sh
```

to install all packages and repos.

Then `cd` into the `build` directory and run all `.sh` files there.
That will build and install all manually compiled tools.


# TODO #
- Add `hdmi_enable_4k=1` to `/boot/config.txt` on Raspberry PIs.
