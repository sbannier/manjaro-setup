#! /usr/bin/env bash

set -e

desktopfile=${1}.desktop

echo "[Desktop Entry]" > ${desktopfile}
echo "Encoding=UTF-8" >> ${desktopfile}
echo "Version=1.0" >> ${desktopfile}
echo "Type=Application" >> ${desktopfile}
echo "Terminal=false" >> ${desktopfile}
echo "Exec=${3}" >> ${desktopfile}
echo "Name=${2}" >> ${desktopfile}
echo "Icon=${4}" >> ${desktopfile}

desktopfile_dir=~/.local/share/applications/
if [ ! -d ${desktopfile_dir} ]; then
    mkdir -p ${desktopfile_dir}
fi
mv ${desktopfile} ${desktopfile_dir}
