#! /usr/bin/env bash

set -e

TAG=30.1
app_name=emacs-${TAG}
prefix=/opt/${app_name}
arch_name=${app_name}.tar.gz

if [ ! -f ${arch_name} ]; then
    curl https://ftp.gnu.org/gnu/emacs/${arch_name} > ${arch_name}
fi
if [ -d ${app_name} ]; then
    rm -r ${app_name}
fi
tar xf ${arch_name}
pushd ${app_name}

./autogen.sh

./configure --with-imagemagick --with-json --with-cairo --with-x=yes --with-x-toolkit=gtk3 --with-toolkit-scroll-bars --with-native-compilation CFLAGS="-O2 -mtune=native -march=native -fomit-frame-pointer" --prefix=${prefix}

make -j4
if [ -d ${prefix} ]; then
    sudo rm -rf ${prefix}
fi
sudo make install

popd

./create_desktopfile.sh ${app_name} "Emacs ${TAG}" /opt/${app_name}/bin/${app_name} /opt/${app_name}/share/icons/hicolor/scalable/apps/emacs.svg
