#! /usr/bin/env bash

set -e

function append_to_file {
    text=$1
    file=$2

    if ! $(grep "$text" "$file"); then
        echo "$text" >> "$file"
    fi
}


sudo pamac update
sudo pamac upgrade

#sudo pamac install --no-confirm git-svn
#sudo pamac install --no-confirm ripperx
sudo pamac install --no-confirm \
     bash-language-server \
     base-devel \
     cmake \
     evolution \
     gimp \
     git \
     global \
     gparted \
     hunspell \
     hunspell-en_us \
     imagemagick \
     jansson \
     jdk-openjdk \
     kdiff3 \
     keepassxc \
     lame \
     leiningen \
     libgccjit \
     libvterm \
     meld \
     mercurial \
     perl-perl-critic \
     python-lsp-server \
     python-pip \
     python-pylint \
     python-virtualenvwrapper \
     ripgrep \
     subversion \
     texlive-core \
     texlive-fontsextra \
     texlive-formatsextra \
     texlive-langextra \
     the_silver_searcher \
     ttf-font-awesome \
     ttf-ibm-plex \
     udiskie \
     yamllint

append_to_file "# Python virtualenvwrapper" ~/.bashrc
append_to_file "export WORKON_HOME=~/.virtualenvs" ~/.bashrc
append_to_file "source /usr/bin/virtualenvwrapper_lazy.sh" ~/.bashrc

append_to_file "# Disable freezing console with CTRL-S" ~/.bashrc
append_to_file "stty -ixon" ~/.bashrc
